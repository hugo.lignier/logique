#ifndef NXORGATE
#define NXORGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"

using namespace std ;

class NXorGate : public Gate{
public:
	NXorGate(vector<Gate*> const &);

	virtual ~NXorGate();

	deque<bool> dequeResult() override; // calcule result et le renvoie


};

#endif