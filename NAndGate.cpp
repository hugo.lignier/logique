#include "NAndGate.hpp"
#include "AndGate.hpp"
#include "Gate.hpp"
#include "InputGate.hpp"
#include "NegateGate.hpp"


NAndGate::NAndGate(vector<Gate*> const & v) : Gate(v){
	setName("NAnd");
}

NAndGate::~NAndGate(){}

	//protected vector<InputGate> vig;
	//protected vector<Gate> vg;

deque<bool> NAndGate::dequeResult(){

	AndGate *and1 = new AndGate(this->getVec());
	deque<bool> base = and1->dequeResult();
	InputGate *baseBit = new InputGate(this->getName(),base);
	NegateGate negateBase({baseBit});
	//delete or1;
	//delete baseBis;

	expFinal = negateBase.dequeResult() ; // la base a été modifiée
	return expFinal;
}

int main(){
	InputGate *a = new InputGate("a",{1,0,0,1,1,1});
	InputGate *b = new InputGate("b",{1,1,0,0,0,1});
	NAndGate nand1({a,b});
	nand1.print();

	deque<bool> e= nand1.dequeResult();
	for(bool b : e){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;

	return 0;
}
