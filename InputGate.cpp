#include "InputGate.hpp"

InputGate::InputGate(string const & name, deque<bool> const & expression) : Gate(name,expression){}

InputGate::InputGate(string const & name) : InputGate(name, {} ) {}

InputGate::InputGate() : InputGate("", {}) {}

InputGate::~InputGate(){}

deque<bool> InputGate::getExpFinal() const{
	return expFinal ;
}

void InputGate::setExpFinal(deque<bool> const & expression){
	expFinal = expression ;
}

bool InputGate::ifInputGate() const{
	return true;
}

deque<bool> InputGate::dequeResult(){
	return expFinal ;
}

/*ostream & operator<<(ostream & out, InputGate const * i){
	out << "name : " << i->getName() << endl ;
	out << "expression : " ;
	int taille = expFinal.size();
	for(unsigned int j=0; j<taille; j++){
		if(expFinal[j]){
			out << " 1";
		}
		else{
			out << " 0";
		}
	}
	out << endl;
	return out;
}*/

void InputGate::printBis() const{
	cout << name << " : ";
	for( bool b : expFinal ){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
}

/** test
int main(){
	deque<bool> d1 = {false, true, true};
	deque<bool> d2 = {false, true };
	InputGate *a = new InputGate("a");
	a->setExpFinal(d1);
	cout << a << endl;

	InputGate *b = new InputGate("ze",d1);
	cout << b << endl;

	b->setExpFinal(d2);
	cout << b << endl;
	return 0;
}*/