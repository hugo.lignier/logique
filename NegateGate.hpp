#ifndef NEGATEGATE
#define NEGATEGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"

using namespace std ;

class NegateGate : public Gate{
public:
	NegateGate(vector<Gate*> const &);

	virtual ~NegateGate();

	deque<bool> dequeResult() override; // calcule result et le renvoie

};

#endif