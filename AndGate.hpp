#ifndef ANDGATE
#define ANDGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"

using namespace std ;

class AndGate : public Gate{
public:
	AndGate(vector<Gate*> const &);

	virtual ~AndGate();

	deque<bool> dequeResult() override; // calcule result et le renvoie


};

#endif