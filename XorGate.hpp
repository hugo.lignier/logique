#ifndef XORGATE
#define XORGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"
using namespace std;


class XorGate : public Gate{
public:
	XorGate(vector<Gate*> const &);

	virtual ~XorGate();

deque<bool> dequeResult() override;


};

#endif

