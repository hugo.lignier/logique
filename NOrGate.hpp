#ifndef NORGATE
#define NORGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"

using namespace std ;

class NOrGate : public Gate{
public:
	NOrGate(vector<Gate*> const &);

	virtual ~NOrGate();

	deque<bool> dequeResult() override; // calcule result et le renvoie


};

#endif