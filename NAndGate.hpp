#ifndef NANDGATE
#define NANDGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"

using namespace std ;

class NAndGate : public Gate{
public:
	NAndGate(vector<Gate*> const &);

	virtual ~NAndGate();

	deque<bool> dequeResult() override; // calcule result et le renvoie


};

#endif