#ifndef GATE
#define GATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

using namespace std ;

//class InputGate; // pour pouvoir utiliser InputGate en tant que type dans le .cpp

class Gate{
protected:

	string name;
	vector<Gate*> vg;
	deque<bool> expFinal;

	Gate(vector<Gate*> const &);
	Gate();
	Gate(deque<bool> const &);
	Gate(string const &, deque<bool> const &);

	void setName(string const &);


	int sizeMax() const;

	

	

public:


	vector<Gate*> getVec() const;
	string getName() const;
	virtual ~Gate();
	virtual void printBis() const;
	void print() const;
	virtual bool ifInputGate() const;
	virtual deque<bool> dequeResult() = 0; // calcule expFinal et la renvoie

};

#endif