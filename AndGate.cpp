#include "AndGate.hpp"
#include "Gate.hpp"
#include "InputGate.hpp"

AndGate::AndGate(vector<Gate*> const & v) : Gate(v){
	setName("And");
}

AndGate::~AndGate(){}

	//protected vector<InputGate> vig;
	//protected vector<Gate> vg;

deque<bool> AndGate::dequeResult(){

	int max = this->sizeMax();

	deque<bool> base ; // crétation d'une base
	for(unsigned int j=0;j<max;j++){
		base.push_back(true); // une base de 111111....1 avec la taille max
	}

	deque<bool> e;
		
	// écrire la suite avec vector<Gate> : même principe

	for(Gate *g: vg){
		if(g->ifInputGate()){
			e = (dynamic_cast<InputGate*>(g))->getExpFinal(); // downcast controlé sur pointeur
			for(unsigned int k=0;k<max-e.size();k++){
				e.push_front(false); // padding sur l'avant
			}

			for(unsigned int j=0;j<max;j++){
				base.at(j) = base.at(j) && e.at(j);
				}
			}
		else{
			base = g->dequeResult();
		}	
	}

	expFinal = base ; // la base a été modifiée
	return expFinal;
}



/*int main(){
	InputGate *a = new InputGate("a",{1,0,0,1,1,1});
	InputGate *b = new InputGate("b",{1,0,0,1,1,1});
	AndGate and1({a,b});
	and1.print();
	deque<bool> e = and1.dequeResult(); //bon résultat
	for(bool b : e){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;


	return 0;
}*/