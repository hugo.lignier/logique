#include "Gate.hpp"
#include "InputGate.hpp"

Gate::Gate(vector<Gate*> const & v) : name{"Gate"}, vg{v} , expFinal({}) {}

Gate::Gate() : name{""}, vg{nullptr}, expFinal({}) {}

Gate::Gate(deque<bool> const & expression) : name{"Gate"} , vg{nullptr} , expFinal({}) {}

Gate::Gate(string const & name, deque<bool> const & expression) : name{name} , vg{nullptr} , expFinal{expression} {}


Gate::~Gate(){
	for(Gate *g: vg){
		delete g;
	}
}

bool Gate::ifInputGate() const{
	return false;
}

int Gate::sizeMax() const{
	int max = 0 ;
	int taille=0 ;
	vector<Gate*> v ;
	//cout << "coucou" << endl;

	for(Gate *g : vg){
		//cout << "je rentre dans le for" << endl;
		//cout <<"g est un nullptr : " << (g == nullptr) << endl;
		//cout << "adresse de g : "<< g << ", g est un " << g->getName() << endl;
		//cout << "g est un InputGate : " << g->ifInputGate() << endl;
		if(g==nullptr){
			taille=0;
		}
		else if(g->ifInputGate()){
			taille = (dynamic_cast<InputGate*>(g))->getExpFinal().size(); //downcast contrôlé
			if(max<taille){
				
				max = taille;
			}
		}
		else{
			//cout << "je suis dans else" << endl;
			v = g->getVec();
			for(Gate *g : v ){
				taille = g->sizeMax(); // recursion;
				if(max<taille){
					max = taille;
				}
			}
		}
	}

	return max;
}

/*int Gate::sizeAcc() const{
	int acc = 0 ;
	int taille=0 ;
	vector<Gate*> v ;

	for(Gate *g : vg){
		if(g->ifInputGate()){
			taille = (dynamic_cast<InputGate*>(g))->getExpFinal().size(); //downcast contrôlé
			acc =+ taille;
		}
		else{
			v = g->getVec();
			for(Gate *g : v ){
				if(g->ifNegate()){
					taille = g->sizeAcc();
					acc =+ taille;
				}else{
					taille = g->sizeMax();
					acc =+ taille;
				}
			}
		}
	}

	return acc;
}*/

string Gate::getName() const{
	return name;
}

void Gate::setName(string const & s){
	name = s;
}

vector<Gate*> Gate::getVec() const{
	return vg;
}

void Gate::printBis() const{

	int taille = vg.size();

	cout << getName() << "(";
	for(unsigned int i=0; i<taille-1 ;i++){
		vg.at(i)->printBis();
		cout << ", ";
	}
	if(taille-1>=0){
		vg.at(taille-1)->printBis();
		cout << ")";
	}

}

void Gate::print() const{
	this->printBis();
	cout << endl;
}

/*int main(){
	InputGate a1("a1");
	InputGate a2("a2");
	InputGate a3("a3");
	vector<InputGate> v1 = {a1, a2, a3};
	vector<InputGate> v2 = {a1, a3};
	Gate g1(v1);
	Gate g2(v2);
	Gate g3({g1,g2});
	return 0;
}*/
