#include "OrGate.hpp"
#include "Gate.hpp"
#include "InputGate.hpp"

OrGate::OrGate(vector<Gate*> const & v) : Gate(v){
	setName("Or");
}

OrGate::~OrGate(){}

	//protected vector<InputGate> vig;
	//protected vector<Gate> vg;

deque<bool> OrGate::dequeResult(){

	int max = this->sizeMax();

	deque<bool> base ; // crétation d'une base
	for(unsigned int j=0;j<max;j++){
		base.push_back(false); // une base de 00....0 avec la taille max
	}

	deque<bool> e;
		
	// écrire la suite avec vector<Gate> : même principe

	for(Gate *g: vg){
		if(g->ifInputGate()){
			e = (dynamic_cast<InputGate*>(g))->getExpFinal(); // downcast controlé sur pointeur
			for(unsigned int k=0;k<max-e.size();k++){
				e.push_front(false); // padding sur l'avant
			}

		for(unsigned int j=0;j<max;j++){
			base.at(j) = base.at(j) || e.at(j);
			}
		}
		else{
			base = g->dequeResult();
		}	
	}

	expFinal = base ; // la base a été modifiée
	return expFinal;
}



int main(){
	InputGate *a = new InputGate("a",{1,0,0,1,1,1});
	InputGate *b = new InputGate("b",{1,1,0,0,0,1});
	OrGate or1({a,b});
	or1.print();
	deque<bool> e = or1.dequeResult(); //bon résultat
	for(bool b : e){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;


	return 0;
}