#ifndef ORGATE
#define ORGATE

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "Gate.hpp"

using namespace std ;

class OrGate : public Gate{
public:
	OrGate(vector<Gate*> const &);

	virtual ~OrGate();

	deque<bool> dequeResult() override; // calcule result et le renvoie


};

#endif