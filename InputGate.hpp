#ifndef INPUTGATE
#define INPUTGATE

#include "Gate.hpp"

#include <iostream>
#include <string>
#include <deque>

using namespace std;


class InputGate: public Gate{

public:
	InputGate();
	InputGate(string const &, deque<bool> const &);
	InputGate(string const &);
	virtual ~InputGate();

	void setExpFinal(deque<bool> const &);
	deque<bool> getExpFinal() const;

	void printBis() const override;
	bool ifInputGate() const override;

	deque<bool> dequeResult() override;
 
};

//ostream & operator<<(ostream &, InputGate const &);

#endif
