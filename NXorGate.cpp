#include "NXorGate.hpp"
#include "XorGate.hpp"
#include "Gate.hpp"
#include "InputGate.hpp"
#include "NegateGate.hpp"


NXorGate::NXorGate(vector<Gate*> const & v) : Gate(v){
	setName("NXor");
}

NXorGate::~NXorGate(){}

	//protected vector<InputGate> vig;
	//protected vector<Gate> vg;

deque<bool> NXorGate::dequeResult(){

	XorGate *xor1 = new XorGate(this->getVec());
	deque<bool> base = xor1->dequeResult();
	InputGate *baseBit = new InputGate(this->getName(),base);
	NegateGate negateBase({baseBit});
	//delete or1;
	//delete baseBis;

	expFinal = negateBase.dequeResult() ; // la base a été modifiée
	return expFinal;
}

int main(){
	InputGate *a = new InputGate("a",{1,0,0,1,1,1});
	InputGate *b = new InputGate("b",{1,1,0,0,0,1});
	NXorGate nxor1({a,b});
	nxor1.print();

	deque<bool> e= nxor1.dequeResult();
	for(bool b : e){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;

	return 0;
}