#include "NegateGate.hpp"
#include "Gate.hpp"
#include "InputGate.hpp"
#include "AndGate.hpp"

NegateGate::NegateGate(vector<Gate*> const & v) : Gate(v){
	setName("Negate");
}

NegateGate::~NegateGate(){}

	//protected vector<InputGate> vig;
	//protected vector<Gate> vg;

deque<bool> NegateGate::dequeResult(){

	if(vg.size()==1){

		Gate* g = vg[0];
		deque<bool> base = g->dequeResult(); // création d'une base
		deque<bool>::iterator it;
		int i = 0;
		for(it=base.begin(); it<base.end() ;it++){
			if(*it){
				base.at(i) = false;
			}
			else{
				base.at(i) = true;
			}
			i++;
		}
		expFinal = base ; // la base a été modifiée
		return expFinal;

	}
	else{
		cerr << "nombre de vecteurs incohérent" << endl;
		return {}; // on ne fait pas un negate de plusieurs Gates
	}

}



int main(){
	InputGate *a = new InputGate("a",{1,0,0,1,1,1});
	InputGate *b = new InputGate("b",{1,1,0,0,0,1});
	//NegateGate negate1({a});
	//negate1.print();
	/*deque<bool> e = negate1.dequeResult(); //bon résultat
	a->print();
	for(bool b : e){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;*/

	AndGate *and1 = new AndGate({a,b});
	and1->print();
	NegateGate negate2({and1});
	
	deque<bool> e2=negate2.dequeResult();
	for(bool b : e2){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;


	return 0;
}