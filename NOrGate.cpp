#include "NOrGate.hpp"
#include "Gate.hpp"
#include "OrGate.hpp"
#include "NegateGate.hpp"
#include "InputGate.hpp"

NOrGate::NOrGate(vector<Gate*> const & v) : Gate(v){
	setName("NOr");
}

NOrGate::~NOrGate(){}

	//protected vector<InputGate> vig;
	//protected vector<Gate> vg;

deque<bool> NOrGate::dequeResult(){
	
	OrGate *or1 = new OrGate(this->getVec());
	deque<bool> base = or1->dequeResult();
	InputGate *baseBis = new InputGate(this->getName(),base);
	NegateGate negateBase({baseBis});
	//delete or1;
	//delete baseBis;

	expFinal = negateBase.dequeResult() ; // la base a été modifiée
	return expFinal;
}



int main(){
	InputGate *a = new InputGate("a",{1,0,0,1,1,1});
	InputGate *b = new InputGate("b",{1,1,0,0,0,1});
	NOrGate nor1({a,b});
	nor1.print();

	deque<bool> e= nor1.dequeResult();
	for(bool b : e){
		if(b){
			cout << "1";
		}
		else{
			cout << "0";
		}
	}
	cout << endl;

	return 0;
}